#!/bin/bash

set -x
echo Y > /sys/module/zswap/parameters/enabled
echo zsmalloc > /sys/module/zswap/parameters/zpool
echo lz4 > /sys/module/zswap/parameters/compressor
echo madvise > /sys/kernel/mm/transparent_hugepage/enabled

mount -t tmpfs none /sys/fs/cgroup
mkdir /sys/fs/cgroup/unified
mount -t cgroup2 none /sys/fs/cgroup/unified
mkdir /sys/fs/cgroup/unified/0
echo "+memory" > /sys/fs/cgroup/unified/cgroup.subtree_control

