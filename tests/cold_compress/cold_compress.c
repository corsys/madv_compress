#include <stdio.h>
#include <stdlib.h>
#include <errno.h>
#include <unistd.h>
#include <sys/mman.h>
#include <stdbool.h>
#include <time.h>
#include <asm-generic/mman-common.h>

#define MIN_SIZE 0
#define MAX_SIZE 31
#define PAGE_SIZE (1<<12)
#define SEED 11031985
#define MADV_COMPRESS 22

#define CGROUP_ROOT "/sys/fs/cgroup/unified/0"
#define CGROUP_TASKS (CGROUP_ROOT "/cgroup.procs")
#define CGROUP_MEM_HIGH (CGROUP_ROOT "/memory.high")
#define CGROUP_MEM_MAX (CGROUP_ROOT "/memory.max")
#define CGROUP_MEM_CURRENT (CGROUP_ROOT "/memory.current")
#define CGROUP_MEM_COMPRESS (CGROUP_ROOT "/memory.compress")
#define PROC_SELF_STATM "/proc/self/statm"
#define ZSWAP_STORED_PAGES "/sys/kernel/debug/zswap/stored_pages"
#define ZSWAP_POOL_TOTAL_SIZE "/sys/kernel/debug/zswap/pool_total_size"

const char usage[] =
"Usage: cold_compress HOT_SIZE COLD_SIZE COLD_HIGH COLD_MAX [FILE]\n"
"Test zswap with a memory cgroup with the given sizes and input data.\n"
"\n"
"The application allocates 2^X bytes for each size argument, where X is\n"
"HOT_SIZE or COLD_SIZE. The decompressed area is limited to 2^COLD_HIGH\n"
"bytes. If it goes over 2^COLD_MAX bytes, Linux will OOM kill it.\n"
"\n"
"HOT_SIZE, COLD_SIZE, COLD_HIGH, and COLD_MAX must be between %d and %d\n"
"\n"
"FILE is the file containing data to compress.\n"
;

extern int errno;

int get_rss(void)
{
  FILE *statm;
  int tmp, rss;

  statm = fopen("/proc/self/statm", "r");
  if (fscanf(statm, "%d %d", &tmp, &rss) != 2) {
    printf("error reading statm\n");
    return 0;
  }
  return rss;
}

unsigned long long get_stat(const char *fname, int pos)
{
  FILE *fptr;
  int cnt;
  unsigned long long tmp, stat;

  fptr = fopen(fname, "r");
  if (!fptr) {
    fprintf(stderr, "Error opening %s\n", fname);
    perror("");
    exit(errno);
  }

  while (cnt < pos) {
    if ( (fscanf(fptr, "%llu", &stat)) != 1 ) {
      fprintf(stderr, "Error reading %s\n", fname);
      exit(-EINVAL);
    }
    cnt++;
  }
  fclose(fptr);

  return stat;
}

#if 0
void print_stats(void)
{
  unsigned long long rss, high, max, dsize, psize, csize;

  rss   = (get_stat(PROC_SELF_STATM, 2) * PAGE_SIZE);
  high  = get_stat(CGROUP_MEM_HIGH, 1);
  max   = get_stat(CGROUP_MEM_MAX, 1);
  dsize = get_stat(CGROUP_MEM_CURRENT, 1);
  csize = (get_stat(ZSWAP_STORED_PAGES, 1) * PAGE_SIZE);
  psize = get_stat(ZSWAP_POOL_TOTAL_SIZE, 1);
  
  printf("rss: %-12llu high: %-12llu max: %-12llu dsize: %-12llu csize: %-12llu psize: %-12llu\n",
         rss, high, max, dsize, csize, psize);
}
#endif

/* Subtracts two timespec structs from each other. Assumes stop is
 * larger than start.
 */
static void timespec_diff(struct timespec *start, struct timespec *stop,
                   struct timespec *result) {
  if ((stop->tv_nsec - start->tv_nsec) < 0) {
    result->tv_sec = stop->tv_sec - start->tv_sec - 1;
    result->tv_nsec = stop->tv_nsec - start->tv_nsec + 1000000000;
  } else {
    result->tv_sec = stop->tv_sec - start->tv_sec;
    result->tv_nsec = stop->tv_nsec - start->tv_nsec;
  }
}

void get_elapsed_str(struct timespec *start, char *buf)
{
  struct timespec cur, elapsed;

  clock_gettime(CLOCK_MONOTONIC, &cur);
  timespec_diff(start, &cur, &elapsed);
  sprintf(buf, "%ld.%03ld", elapsed.tv_sec, (elapsed.tv_nsec / 1000000));
}

void print_stats(void)
{
  unsigned long long rss, cmp, d_rss, p_rss;

  rss   = (get_stat(PROC_SELF_STATM, 2) * PAGE_SIZE);
  cmp   = (get_stat(ZSWAP_STORED_PAGES, 1) * PAGE_SIZE);
  d_rss = get_stat(CGROUP_MEM_CURRENT, 1);
  p_rss = get_stat(ZSWAP_POOL_TOTAL_SIZE, 1);
  
  printf("%12llu%12llu%12llu%12llu",
         rss, cmp, d_rss, p_rss);
}

ssize_t get_size_arg(char *arg)
{
  ssize_t size;

  size = atoi(arg);
  if ( (size < MIN_SIZE) || (size > MAX_SIZE) ) {
    printf(usage, MIN_SIZE, MAX_SIZE);
    return -EINVAL;
  }

  return size;
}

void parse_arguments(int argc, char **argv, ssize_t *hot_size,
  ssize_t *cold_size, ssize_t *cold_high, ssize_t *cold_max, char **fname)
{
  if (((argc < 5) || (argc > 6))) {
    printf(usage, MIN_SIZE, MAX_SIZE);
    exit(1);
  }

  (*hot_size)  = get_size_arg(argv[1]);
  (*cold_size) = get_size_arg(argv[2]);
  (*cold_high) = get_size_arg(argv[3]);
  (*cold_max)  = get_size_arg(argv[4]);

  (*fname) = NULL;
  if (argc == 6) {
    (*fname) = argv[5];
  }
}

void add_self_to_cgroup(void)
{
  char buf[10];
  FILE *cgroup_tasks;

  cgroup_tasks = fopen(CGROUP_TASKS, "w");
  if (!cgroup_tasks) {
    fprintf(stderr, "Error opening %s\n", CGROUP_TASKS);
    perror("");
    exit(errno);
  }

  sprintf(buf, "%d", getpid());
  fputs(buf, cgroup_tasks);
  fclose(cgroup_tasks);
}

/* only concern ourselves with MADV_COMRPESS'd data */
void cgroup_set_mem_compress(void)
{
  FILE *cgroup_mem_compress;
  cgroup_mem_compress = fopen(CGROUP_MEM_COMPRESS, "w");
  if (!cgroup_mem_compress) {
    fprintf(stderr, "Error opening %s\n", CGROUP_MEM_COMPRESS);
    perror("");
    exit(errno);
  }
  fputs("Y", cgroup_mem_compress);
  fclose(cgroup_mem_compress);
}

void cgroup_set_mem_high(size_t nr_bytes)
{
  char buf[20];
  FILE *cgroup_mem_high;

  cgroup_mem_high = fopen(CGROUP_MEM_HIGH, "w");
  if (!cgroup_mem_high) {
    fprintf(stderr, "Error opening %s\n", CGROUP_MEM_HIGH);
    perror("");
    exit(errno);
  }

  sprintf(buf, "%llu", nr_bytes);
  fputs(buf, cgroup_mem_high);
  fclose(cgroup_mem_high);
}

void cgroup_set_mem_max(size_t nr_bytes)
{
  char buf[20];
  FILE *cgroup_mem_max;

  cgroup_mem_max = fopen(CGROUP_MEM_MAX, "w");
  if (!cgroup_mem_max) {
    fprintf(stderr, "Error opening %s\n", CGROUP_MEM_MAX);
    perror("");
    exit(errno);
  }

  sprintf(buf, "%llu", nr_bytes);
  fputs(buf, cgroup_mem_max);
  fclose(cgroup_mem_max);
}

char *get_byte_array(size_t nr_bytes, char *fname, bool compress)
{
  FILE *fptr;
  char *mem, cur_char;
  size_t done, i, j;

  mem = (char*) mmap(0, nr_bytes, PROT_READ|PROT_WRITE,
                     MAP_PRIVATE|MAP_ANONYMOUS, -1, 0);

  if (mem == MAP_FAILED) {
    exit(-ENOMEM);
  }

#if 1
  if (compress) {
    printf("compressing the cold array ... ");
    if ( (madvise( (&(mem[0])), nr_bytes, MADV_COMPRESS )) != 0 ) {
      perror("madvise error");
      exit(errno);
    }
    printf("done\n");
  }
#endif

  fptr = NULL;
  if (fname) {
    fptr = fopen(fname, "rb");
    if (!fptr) {
      fprintf(stderr, "Warning: could not open %s\n", fname);
      perror("");
    }
  }

  if (fptr) {

    /* Initialize the array with data from the given file. If the file
     * size < nr_bytes, circle back to the beginning of the file and fill the
     * remaining addresses. If the file size >= nr_bytes, this loop will only
     * execute once.
     */

    done = 0;
    do {
      fseek(fptr, 0L, SEEK_SET);
      done += fread(&(mem[done]), 1, nr_bytes, fptr);
    } while (done < nr_bytes);
    fclose(fptr);

  } else {

    /* No file data, so initialize the array using a simple, compressible
     * pattern
     */
    cur_char = 'a';
    for (i = 0 ; i < nr_bytes; i+=8) {
      for (j = 0; j < 8; j++) {
        mem[(i+j)] = cur_char;
      }
      cur_char = (char)((int)cur_char + 1);
    }
  }

  return mem;
}

int main (int argc, char **argv)
{
  char *hot_arr, *cold_arr, *fname, timebuf[14];
  ssize_t hot_size,  cold_size,  cold_high, cold_max;
  size_t  hot_bytes, cold_bytes, high_bytes, max_bytes, i, j;
  unsigned long long count;
  struct timespec start;

  parse_arguments(argc, argv, &hot_size, &cold_size, &cold_high,
                  &cold_max, &fname);

  hot_bytes  = (1ull << hot_size);
  cold_bytes = (1ull << cold_size);
  high_bytes = (1ull << cold_high);
  max_bytes  = (1ull << cold_max);

  clock_gettime(CLOCK_MONOTONIC, &start);

  if (cold_high != 0) {
    add_self_to_cgroup();
    cgroup_set_mem_compress();
    cgroup_set_mem_high(high_bytes);
    cgroup_set_mem_max(max_bytes);
    printf("keeping a limit of 2^%d bytes in the decompress area\n", cold_high);
  }

  printf("allocating hot array (2^%d bytes) ... ", hot_size);
  hot_arr = get_byte_array(hot_bytes, fname, false);
  printf("done\n");

  printf("allocating cold array (2^%d bytes) ... ", cold_size);
  cold_arr = get_byte_array(cold_bytes, fname, (cold_high!=0));
  printf("done\n");


#if 0
  if (cold_high != 0) {
    printf("compressing the cold array ... ");
    if ( (madvise( (&(cold_arr[0])), cold_bytes, MADV_COMPRESS )) != 0 ) {
      perror("madvise error");
      return errno;
    }
    printf("done\n");
  }
#endif

  printf("accessing the arrays ...\n");
  printf("  %-6s%12s%12s%12s%12s%12s\n", "pos", "rss", "cmp", "d_rss", "p_rss", "time (s)");
  count = 0;
  for (i = 0; i < cold_bytes; i+=(sizeof(unsigned long long))) {
    for (j = 0; j < hot_bytes; j+=(sizeof(unsigned long long))) {
      count += (*((unsigned long long*) &(hot_arr[j])));
    }
    count *= (*((unsigned long long*) &(cold_arr[i])));

    if ( (i % (1<<23)) == 0 ) {
      get_elapsed_str(&start, &(timebuf[0]));
      printf("  %-6d", (i>>20));
      print_stats();
      printf("%12s\n", timebuf);
    }
  }
  printf("  done.\n");
  printf("final count: %llu\n", count);
}
