#include <stdio.h>
#include <stdlib.h>
#include <errno.h>
#include <unistd.h>
#include <sys/mman.h>
#include <asm-generic/mman-common.h>

#define MIN_SIZE 0
#define MAX_SIZE 32
#define PAGE_SIZE (1<<12)
#define SEED 11031985

const char usage[] =
"Usage: mcompress MEM_SIZE CMP_SIZE CMP_CHAR\n"
"Allocate 2^MEM_SIZE bytes of memory and compress the first 2^CMP_SIZE bytes\n"
"\n"
"The allocated memory is initialized to random values. mcompress counts the\n"
"number of CMP_CHAR characters in the byte array before and after compression.\n"
"\n"
"MEM_SIZE and CMP_SIZE must be between %d and %d\n";

extern int errno;

int get_rss() {
  FILE *statm;
  int tmp, rss;

  statm = fopen("/proc/self/statm", "r");
  if (fscanf(statm, "%d %d", &tmp, &rss) != 2) {
    printf("error reading statm\n");
    return 0;
  }
  return rss;
}

int main (int argc, char **argv) {
  int mem_size, cmp_size, chcnt;
  char *mem, cmp_char, cur_char;
  size_t i, j, mem_bytes, cmp_bytes;

  if (argc != 4) {
    printf(usage, MIN_SIZE, MAX_SIZE);
    return EINVAL;
  }

  mem_size = atoi(argv[1]);
  if ( (mem_size < 0) || (mem_size > 32) ) {
    printf(usage, MIN_SIZE, MAX_SIZE);
    return EINVAL;
  }
  mem_bytes = (1ull << mem_size);

  cmp_size = atoi(argv[2]);
  if ( (cmp_size < 0) || (cmp_size > 32) ) {
    printf(usage, MIN_SIZE, MAX_SIZE);
    return EINVAL;
  }
  cmp_bytes = (1ull << cmp_size);

  cmp_char = argv[3][0];

  srand(SEED);

  printf("allocating 2^%d bytes of memory ... ", mem_size);
  mem = (char*)mmap(0, mem_bytes, PROT_READ|PROT_WRITE, MAP_PRIVATE|MAP_ANONYMOUS, -1, 0);
  if (mem == MAP_FAILED) {
    return ENOMEM;
  }
  printf("done\n");

  printf("initializing 2^%d bytes of memory with random data ... ", mem_size);
  cur_char = cmp_char;
#if 0
  for (i = 0 ; i < mem_bytes; i++) {
    //mem[i] = rand();
    //mem[i] = cmp_char;
  }
#endif
#if 1
  for (i = 0 ; i < mem_bytes; i+=8) {
    for (j = 0; j < 8; j++) {
      mem[(i+j)] = cur_char;
    }
    cur_char = (char)((int)cur_char + 1);
  }
#endif
  printf("done\n");

  printf("cur_rss: %d\n", get_rss());

  chcnt = 0;
  for (i = 0; i < cmp_bytes; i++) {
    if (mem[i] == cmp_char) {
      chcnt++;
    }
  }
  printf("first 2^%d bytes contain %d %c's\n", cmp_size, chcnt, cmp_char);

  sleep(2);
  printf("compressing the first 2^%d bytes of memory ... ", cmp_size);
  if ( (madvise( (&(mem[0])), cmp_bytes, MADV_PAGEOUT )) != 0 ) {
    perror("Error: ");
    return errno;
  }
  printf("done\n");
  printf("cur_rss: %d\n", get_rss());

  sleep(2);
  printf("starting accesses\n");
  chcnt = 0;
  for (i = 0; i < cmp_bytes; i++) {
    if (mem[i] == cmp_char) {
      chcnt++;
    }
  }
  printf("done\n");
  sleep(2);
  printf("first 2^%d bytes contain %d %c's\n", cmp_size, chcnt, cmp_char);
  printf("cur_rss: %d\n", get_rss());

#if 0
  for (i = 0 ; i < mem_bytes; i++) {
    //mem[i] = rand();
    //mem[i] = cmp_char;
  }
#endif
#if 1
  for (i = 0 ; i < mem_bytes; i+=8) {
    for (j = 0; j < 8; j++) {
      mem[(i+j)] = cur_char;
    }
    cur_char = (char)((int)cur_char + 1);
  }
#endif

  sleep(2);
  printf("compressing the first 2^%d bytes of memory ... ", cmp_size);
  if ( (madvise( (&(mem[0])), cmp_bytes, MADV_PAGEOUT )) != 0 ) {
    perror("Error: ");
    return errno;
  }
  printf("done\n");
  printf("cur_rss: %d\n", get_rss());

  sleep(2);
  printf("starting accesses\n");
  chcnt = 0;
  for (i = 0; i < cmp_bytes; i++) {
    if (mem[i] == cmp_char) {
      chcnt++;
    }
  }
  printf("done\n");
  sleep(2);
  printf("first 2^%d bytes contain %d %c's\n", cmp_size, chcnt, cmp_char);
  printf("cur_rss: %d\n", get_rss());
}
