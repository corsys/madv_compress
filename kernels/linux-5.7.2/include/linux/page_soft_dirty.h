#ifndef _LINUX_MM_PAGE_PAGE_SOFT_DIRTY_H
#define _LINUX_MM_PAGE_PAGE_SOFT_DIRTY_H

#include <linux/bitops.h>
#include <linux/page-flags.h>
#include <linux/page_ext.h>

#ifdef CONFIG_MADV_COMPRESS
bool page_soft_dirty_ptes(struct page *page);
void clear_page_soft_dirty_ptes(struct page *page);
#endif /* CONFIG_MADV_COMPRESS */
#endif /* _LINUX_MM_PAGE_SOFT_DIRTY_H */

