#ifndef _LINUX_ZSWAP_H_
#define _LINUX_ZSWAP_H_
#ifdef CONFIG_MADV_COMPRESS
bool zswap_is_enabled(void);
bool zswap_compress(struct page *page, bool *page_incompressible);
void zswap_cc_clear(void);
#endif /* CONFIG_MADV_COMPRESS */
#endif /* _LINUX_ZSWAP_H */

