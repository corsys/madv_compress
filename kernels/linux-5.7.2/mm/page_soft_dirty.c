#include <linux/hugetlb.h>
#include <linux/memblock.h>
#include <linux/mm.h>
#include <linux/mmzone.h>
#include <linux/rmap.h>
#include <linux/swap.h>
#include <linux/swapops.h>
#include <linux/page_ext.h>
#include <linux/mmu_notifier.h>
#include <linux/page_soft_dirty.h>

#ifdef CONFIG_MEM_SOFT_DIRTY
static inline void clear_soft_dirty(struct vm_area_struct *vma,
		unsigned long addr, pte_t *pte)
{
	/*
	 * The soft-dirty tracker uses #PF-s to catch writes
	 * to pages, so write-protect the pte as well. See the
	 * Documentation/admin-guide/mm/soft-dirty.rst for full description
	 * of how soft-dirty works.
	 */
	pte_t ptent = *pte;

	if (pte_present(ptent)) {
		pte_t old_pte;

		old_pte = ptep_modify_prot_start(vma, addr, pte);
		ptent = pte_wrprotect(old_pte);
		ptent = pte_clear_soft_dirty(ptent);
		ptep_modify_prot_commit(vma, addr, pte, old_pte, ptent);
	} else if (is_swap_pte(ptent)) {
		ptent = pte_swp_clear_soft_dirty(ptent);
		set_pte_at(vma->vm_mm, addr, pte, ptent);
	}
}

static inline void clear_soft_dirty_pmd(struct vm_area_struct *vma,
		unsigned long addr, pmd_t *pmdp)
{
	pmd_t old, pmd = *pmdp;

	if (pmd_present(pmd)) {
		/* See comment in change_huge_pmd() */
		old = pmdp_invalidate(vma, addr, pmdp);
		if (pmd_dirty(old))
			pmd = pmd_mkdirty(pmd);
		if (pmd_young(old))
			pmd = pmd_mkyoung(pmd);

		pmd = pmd_wrprotect(pmd);
		pmd = pmd_clear_soft_dirty(pmd);

		set_pmd_at(vma->vm_mm, addr, pmdp, pmd);
	} else if (is_migration_entry(pmd_to_swp_entry(pmd))) {
		pmd = pmd_swp_clear_soft_dirty(pmd);
		set_pmd_at(vma->vm_mm, addr, pmdp, pmd);
	}
}

#if 0
static bool invalid_soft_dirty_vma(struct vm_area_struct *vma, void *arg)
{
	if (vma->vm_flags & VM_SHARED) {
		printk("invalid vm_flags");
		return false;
	}

	return true;
}
#endif

/* for now -- we use the soft dirty bit on the PTE as the PT equivalent of
 * compress dirty
 */
static bool clear_page_soft_dirty_ptes_one(struct page *page,
					struct vm_area_struct *vma,
					unsigned long addr, void *arg)
{
	struct page_vma_mapped_walk pvmw = {
		.page = page,
		.vma = vma,
		.address = addr,
	};
	struct mmu_notifier_range range;

	mmu_notifier_range_init(&range, MMU_NOTIFY_SOFT_DIRTY,
				0, vma, vma->vm_mm, addr,
				min(vma->vm_end, addr + page_size(page)));
	mmu_notifier_invalidate_range_start(&range);

	if (vma->vm_flags & VM_PFNMAP) {
		printk("bad vm_flags");
		return false;
	}

	while (page_vma_mapped_walk(&pvmw)) {

		addr = pvmw.address;
		if (pvmw.pte) {
			clear_soft_dirty(vma, addr, pvmw.pte);
		} else if (IS_ENABLED(CONFIG_TRANSPARENT_HUGEPAGE)) {
			clear_soft_dirty_pmd(vma, addr, pvmw.pmd);
		}
	}

	mmu_notifier_invalidate_range_end(&range);

	return true;
}

void clear_page_soft_dirty_ptes(struct page *page)
{
	/*
	 * Since rwc.arg is unused, rwc is effectively immutable, so we
	 * can make it static const to save some cycles and stack.
	 */
	static const struct rmap_walk_control rwc = {
		.rmap_one = clear_page_soft_dirty_ptes_one,
		.anon_lock = page_lock_anon_vma_read,
	};

	BUG_ON(!PageLocked(page));

	if (!page_mapped(page)) {
		printk("not mapped: %lu", page_to_pfn(page));
		return;
	}

	rmap_walk_locked(page, (struct rmap_walk_control *)&rwc);
}

struct page_soft_dirty_arg {
	bool dirty;
};

static bool page_soft_dirty_ptes_one(struct page *page,
					struct vm_area_struct *vma,
					unsigned long addr, void *arg)
{
	struct page_soft_dirty_arg *psda = arg;
	struct page_vma_mapped_walk pvmw = {
		.page = page,
		.vma = vma,
		.address = addr,
	};

	while (page_vma_mapped_walk(&pvmw)) {
		addr = pvmw.address;

		if (pvmw.pte && pte_soft_dirty(*pvmw.pte)) {
			psda->dirty = true;
		} else if (IS_ENABLED(CONFIG_TRANSPARENT_HUGEPAGE)) {
			if (pvmw.pmd && pmd_soft_dirty(*pvmw.pmd)) {
				psda->dirty = true;
			}
		}
	}

	return true;
}

bool page_soft_dirty_ptes(struct page *page)
{
	struct page_soft_dirty_arg psda = {
		.dirty = false,
	};
	struct rmap_walk_control rwc = {
		.rmap_one = page_soft_dirty_ptes_one,
		.arg = (void *)&psda,
		.anon_lock = page_lock_anon_vma_read,
	};

	BUG_ON(!PageLocked(page));

	if (!page_mapped(page))
		return 0;

	rmap_walk_locked(page, (struct rmap_walk_control *)&rwc);

	return (psda.dirty);
}
#else
bool page_soft_dirty_ptes(struct page *page)
{
	return false;
}

void clear_page_soft_dirty_ptes(struct page *page)
{
}
#endif
