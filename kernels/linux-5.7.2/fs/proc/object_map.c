#ifdef CONFIG_OBJECT_MAP

#include <linux/mm.h>
#include <linux/mempolicy.h>

#define DBUG(fmt, ...) \
	printk(KERN_DEBUG "clear_refs_ranges: " fmt, ##__VA_ARGS__);
#define INFO(fmt, ...) \
	printk(KERN_INFO  "clear_refs_ranges: " fmt, ##__VA_ARGS__);

static struct dentry *
proc_object_map_instantiate(struct dentry *dentry,
		struct task_struct *task, const void *ptr);

static struct dentry *proc_object_map_lookup(struct inode *dir,
		struct dentry *dentry, unsigned int flags);

static struct dentry *
proc_object_map_controller_instantiate(struct dentry *dentry,
		struct task_struct *task, const void *ptr);


static const struct inode_operations proc_object_map_inode_operations = {
	.lookup     = proc_object_map_lookup,
	.permission = proc_fd_permission,
	.setattr    = proc_setattr,
};

static const struct inode_operations proc_object_map_controller_inode_operations = {
	.lookup     = proc_object_map_lookup,
	.permission = proc_fd_permission,
	.setattr    = proc_setattr,
};

ssize_t proc_object_map_read(struct file *file, char __user *buf, size_t count, loff_t *ppos);
ssize_t proc_object_map_write(struct file *file, const char __user *buf, size_t count, loff_t *ppos);
ssize_t proc_object_map_controller_write(struct file *file, const char __user *buf, size_t count, loff_t *ppos);

static const struct file_operations proc_object_map_file_operations = {
	.read  = proc_object_map_read,
	.write = proc_object_map_write,
};

static const struct file_operations proc_object_map_controller_file_operations = {
	.write = proc_object_map_controller_write,
};

static int object_map_d_revalidate(struct dentry *dentry, unsigned int flags);

static const struct dentry_operations tid_object_map_dentry_operations = {
	.d_revalidate = object_map_d_revalidate,
	.d_delete     = pid_delete_dentry,
};

static const struct dentry_operations tid_object_map_controller_dentry_operations = {
	.d_revalidate = object_map_d_revalidate,
	.d_delete     = pid_delete_dentry,
};


struct vm_area_struct * get_vma_for_range(struct task_struct *task, unsigned long start, unsigned long end) {
	struct vm_area_struct *ret;
	struct mm_struct      *mm;

	ret = NULL;

	mm = get_task_mm(task);
	if (!mm) {
		goto out;
	}

	if (down_read_killable(&mm->mmap_sem)) {
		goto out_put_mm;
	}

	ret = find_exact_vma(mm, start, end);

	up_read(&mm->mmap_sem);

out_put_mm:
	mmput(mm);
out:
	return ret;
}

struct objmap_record {
	int n_resident_pages;
	int coop_buff_n_bytes;
};

ssize_t objmap_fill_record(struct objmap_record *record, struct task_struct *task, struct vm_area_struct *vma) {
	ssize_t status;

	status = 0;

	record->n_resident_pages  = vma->objmap.n_resident_pages;
	record->coop_buff_n_bytes = vma->objmap.coop_buff_n_bytes;

	return status;
}

ssize_t proc_object_map_read(struct file *file, char __user *buf, size_t count, loff_t *ppos) {
	struct task_struct    *task;
	struct dentry         *dentry;
	struct mm_struct      *mm;
	unsigned long          vm_start;
	unsigned long          vm_end;
	struct vm_area_struct *vma;
	struct objmap_record   record;
	ssize_t                max_len;
	ssize_t                len;
	ssize_t                record_bytes_written;

	task = get_proc_task(file->f_inode);
	if (!task) {
		DBUG("could not get task in proc_object_map_read()\n");
		len = -ENOENT;
		goto out;
	}

	dentry = file->f_path.dentry;

	if (dname_to_vma_addr(dentry, &vm_start, &vm_end)) {
		DBUG("could not get vma from name in proc_object_map_read()\n");
		len = -ENOENT;
		goto out_put_task;
	}

	vma = get_vma_for_range(task, vm_start, vm_end);
	if (!vma) {
		/*
		 * This commonly occurs when the object has disappeared between the
		 * time the director has been read and the time we get here.
		 * So, we don't need to spam the journal..
		 */
		/*         DBUG("could not get vma from range in proc_object_map_read()\n"); */
		len = -ENOENT;
		goto out_put_task;
	}

	mm = get_task_mm(task);
	if (!mm) {
		len = -ENOENT;
		goto out_put_task;
	}

	if ((len = down_read_killable(&mm->mmap_sem))) {
		goto out_put_mm;
	}

	read_lock(&(vma->objmap.rwlock));

	len = record_bytes_written = 0;

	if (*ppos < sizeof(struct objmap_record)) {
		if ((len = objmap_fill_record(&record, task, vma))) {
			DBUG("error filling record\n");
			goto out_unlock_vma;
		}

		max_len = sizeof(struct objmap_record);
		len     = min(max_len - *ppos, count);

		if (copy_to_user(buf, ((void*)&record) + *ppos, len)) {
			len = -EFAULT;
			DBUG("error copying into user buffer\n");
			goto out_unlock_vma;
		}

		record_bytes_written  = len;
		count                -= len;
		*ppos                += len;
	}

	max_len = sizeof(struct objmap_record) + vma->objmap.coop_buff_n_bytes;
	len     = min(max_len - *ppos, count);

	if (len <= 0) {
		len = len ? 0 : record_bytes_written;
		goto out_unlock_vma;
	}

	if (copy_to_user(buf + record_bytes_written,
				vma->objmap.coop_buff + *ppos - sizeof(struct objmap_record),
				len)) {
		len = -EFAULT;
		DBUG("error copying into user buffer\n");
		goto out_unlock_vma;
	}

	*ppos += len;
	len   += record_bytes_written;

out_unlock_vma:
	read_unlock(&(vma->objmap.rwlock));
out_up_read:
	up_read(&mm->mmap_sem);
out_put_mm:
	mmput(mm);
out_put_task:
	put_task_struct(task);
out:
	return len;
}

#define OBJECT_MAP_WRITE_MAX_BYTES (1024)

ssize_t proc_object_map_write(struct file *file, const char __user *buf, size_t count, loff_t *ppos) {
	struct task_struct    *task;
	struct dentry         *dentry;
	unsigned long          vm_start;
	unsigned long          vm_end;
	struct vm_area_struct *vma;

	if (count > OBJECT_MAP_WRITE_MAX_BYTES) {
		count = -EINVAL;
		goto out;
	}

	task = get_proc_task(file->f_inode);
	if (!task) {
		DBUG("could not get task in proc_object_map_write()\n");
		count = -ENOENT;
		goto out;
	}

	dentry = file->f_path.dentry;

	if (dname_to_vma_addr(dentry, &vm_start, &vm_end)) {
		/*
		 * This commonly occurs when the object has disappeared between the
		 * time the director has been read and the time we get here.
		 * So, we don't need to spam the journal..
		 */
		/*         DBUG("could not get vma from range in proc_object_map_write()\n"); */
		count = -ENOENT;
		goto out_put_task;
	}

	vma = get_vma_for_range(task, vm_start, vm_end);
	if (!vma) {
		count = -ENOENT;
		goto out_put_task;
	}

	write_lock(&(vma->objmap.rwlock));

	if (vma->objmap.coop_buff == NULL) {
		vma->objmap.coop_buff = kmalloc(count, GFP_KERNEL);
		if (vma->objmap.coop_buff == NULL) {
			vma->objmap.coop_buff_n_bytes = 0;
			count = -ENOMEM;
			goto out_unlock;
		}
	} else if (vma->objmap.coop_buff_n_bytes < count) {
		vma->objmap.coop_buff = krealloc(vma->objmap.coop_buff, count, GFP_KERNEL);
		if (vma->objmap.coop_buff == NULL) {
			vma->objmap.coop_buff_n_bytes = 0;
			count = -ENOMEM;
			goto out_unlock;
		}
	}
	vma->objmap.coop_buff_n_bytes = count;

	if (copy_from_user(vma->objmap.coop_buff, buf, count) != 0) {
		DBUG("error copying info from user space to vma->objmap.coop_buff\n");
		count = -EFAULT;
		goto out_unlock;
	}

out_unlock:
	write_unlock(&(vma->objmap.rwlock));
out_put_task:
	put_task_struct(task);
out:
	return count;
}

static struct dentry *
proc_object_map_instantiate(struct dentry *dentry,
		struct task_struct *task, const void *ptr) {
	struct proc_inode *ei;
	struct inode *inode;

	inode = proc_pid_make_inode(dentry->d_sb, task, S_IRUSR | S_IWUSR);

	if (!inode)
		return ERR_PTR(-ENOENT);

	ei = PROC_I(inode);

	inode->i_op   = &proc_object_map_inode_operations;
	inode->i_fop  = &proc_object_map_file_operations;
	inode->i_size = 64;

	d_set_d_op(dentry, &tid_object_map_dentry_operations);
	return d_splice_alias(inode, dentry);
}


static struct dentry *proc_object_map_lookup(struct inode *dir,
		struct dentry *dentry, unsigned int flags) {
	unsigned long vm_start, vm_end;
	struct vm_area_struct *vma;
	struct task_struct *task;
	struct dentry *result;

	result = ERR_PTR(-ENOENT);
	task = get_proc_task(dir);
	if (!task)
		goto out;

	result = ERR_PTR(-EACCES);
	if (!ptrace_may_access(task, PTRACE_MODE_READ_FSCREDS))
		goto out_put_task;

	if (strcmp(dentry->d_name.name, "controller") == 0) {
		result = proc_object_map_controller_instantiate(dentry, task, NULL);
		goto out_put_task;
	}

	result = ERR_PTR(-ENOENT);
	if (dname_to_vma_addr(dentry, &vm_start, &vm_end))
		goto out_put_task;

	vma = get_vma_for_range(task, vm_start, vm_end);
	if (!vma)
		goto out_put_task;

	/* @vma */
	if (!vma->is_object_map_entry) {
		goto out_put_task;
	}

	result = proc_object_map_instantiate(dentry, task, NULL);

out_put_task:
	put_task_struct(task);
out:
	return result;
}

#define CONTROLLER_MAGIC (0x0BC01234)

#define CONTROLLER_CMD_ADD     (1)
#define CONTROLLER_CMD_DEL     (2)
#define CONTROLLER_CMD_SETNODE (3)

__attribute__((packed))
	struct object_map_controller_info {
		unsigned int       _magic;
		int                cmd;
		int                node;
		unsigned long long range_start;
		unsigned long long range_end;
	};


static int object_map_split_vma(struct vm_area_struct *vma, struct vm_area_struct **prev, unsigned long start, unsigned long end) {
	struct mm_struct *mm;
	int               error;

	mm    = vma->vm_mm;
	error = 0;

	if (vma->is_object_map_entry) {
		*prev = vma;
		goto out;
	}

	*prev = vma;

	if (start != vma->vm_start) {
		if (unlikely(mm->map_count >= sysctl_max_map_count)) {
			error = -ENOMEM;
			goto out;
		}

		if ((error = __split_vma(mm, vma, start, 1))) { goto out; }
	}

	if (end != vma->vm_end) {
		if (unlikely(mm->map_count >= sysctl_max_map_count)) {
			error = -ENOMEM;
			goto out;
		}
		if ((error = __split_vma(mm, vma, end, 0))) { goto out; }
	}

out:
	return error;
}

ssize_t object_map_controller_del_range(struct object_map_controller_info *info, struct task_struct *task) {
	ssize_t                status;
	struct mm_struct      *mm;
	unsigned long long     start;
	struct vm_area_struct *vma;
	struct vm_area_struct *prev;

	status = 0;
	mm     = NULL;

	mm = get_task_mm(task);
	if (!mm) {
		status = -ENOENT;
		goto out;
	}

	down_write(&mm->mmap_sem);

	if (!mm->mmap) {
		DBUG("mm has no mmap\n");
		status = -ENOENT;
		goto out_up_write;
	}

	start = info->range_start;

	prev = NULL;
	vma  = find_vma_prev(mm, start, &prev);

	if (!vma) {
		status = -ENOENT;
		goto out_up_write;
	}
	if (!vma->is_object_map_entry) {
		status = -EINVAL;
		goto out_up_write;
	}

	write_lock(&(vma->objmap.rwlock));

	vma->is_object_map_entry = 0;

	if (vma->objmap.coop_buff != NULL) {
		kfree(vma->objmap.coop_buff);
		vma->objmap.coop_buff_n_bytes = 0;
	}

	if (prev) {
		vma_merge(mm, prev, prev->vm_start, vma->vm_end,
				vma->vm_flags, vma->anon_vma, vma->vm_file, 0, vma_policy(vma), vma->vm_userfaultfd_ctx);
	}

	write_unlock(&(vma->objmap.rwlock));

out_up_write:
	up_write(&mm->mmap_sem);
	mmput(mm);
out:
	return status;
}

ssize_t object_map_controller_set_node(struct object_map_controller_info *info, struct task_struct *task) {
	ssize_t                status;
	struct mm_struct      *mm;

	status = 0;
	mm     = NULL;

	mm = get_task_mm(task);
	if (!mm) {
		status = -ENOENT;
		goto out;
	}

	status = object_map_migrate_vma(task, mm, info->range_start, info->node);

	if (status) {
		DBUG("vma migration failed -- error %d\n", status);
	}

	mmput(mm);
out:
	return status;
}

/* Call pte_unmap() after using the returned pte_t. */
static pte_t * vpage_to_pte(struct mm_struct *mm, unsigned long vpage) {
	pgd_t *pgd;
	p4d_t *p4d;
	pud_t *pud;
	pmd_t *pmd;
	pte_t *pte;

#define CHECK_OR_RETURN_NULL(which) \
	if (which##_none(*which) || which##_bad(*which)) { return NULL; }

	pgd = pgd_offset(mm, vpage);  CHECK_OR_RETURN_NULL(pgd);
	p4d = p4d_offset(pgd, vpage); CHECK_OR_RETURN_NULL(p4d);
	pud = pud_offset(p4d, vpage); CHECK_OR_RETURN_NULL(pud);
	pmd = pmd_offset(pud, vpage); CHECK_OR_RETURN_NULL(pmd);
	pte = pte_offset_map(pmd, vpage);

	return pte;
}

ssize_t object_map_controller_add_range(struct object_map_controller_info *info, struct task_struct *task) {
	int                    status;
	struct mm_struct      *mm;
	struct vm_area_struct *vma;
	struct vm_area_struct *entry_vma;
	struct vm_area_struct *prev;
	unsigned long long     start;
	unsigned long long     end;
	unsigned long long     tmp;
	int                    did_mark;
	unsigned long          vpage;
	pte_t                 *pte;

	status   = 0;
	mm       = NULL;
	did_mark = 0;

	mm = get_task_mm(task);
	if (!mm) {
		status = -ENOENT;
		goto out;
	}

	down_write(&mm->mmap_sem);

	if (!mm->mmap) {
		DBUG("mm has no mmap\n");
		status = -ENOENT;
		goto out_up_write;
	}

	start = info->range_start;
	end   = info->range_end;

	/*
	 * If the interval [start,end) covers some unmapped address
	 * ranges, just ignore them, but return -ENOMEM at the end.
	 * - different from the way of handling in mlock etc.
	 */
	vma = find_vma_prev(mm, start, &prev);
	if (vma && start > vma->vm_start) {
		prev = vma;
	}

	for (;;) {
		/* Still start < end. */
		status = -ENOMEM;

		if (!vma) { goto loop_out; }

		/* Here start < (end|vma->vm_end). */
		if (start < vma->vm_start) {
			start = vma->vm_start;
			if (start >= end) { goto loop_out; }
		}

		/* Here vma->vm_start <= start < (end|vma->vm_end) */
		tmp = vma->vm_end;
		if (end < tmp) { tmp = end; }

		/* Here vma->vm_start <= start < tmp <= (end|vma->vm_end). */
		status = object_map_split_vma(vma, &prev, start, tmp);

		if (status) { goto loop_out; }

		if (!did_mark && vma->vm_start == start && vma->vm_end == end) {
			entry_vma = vma;
			rwlock_init(&entry_vma->objmap.rwlock);
			write_lock(&(entry_vma->objmap.rwlock));
			entry_vma->is_object_map_entry = 1;
			did_mark                       = 1;
		}

		start = tmp;

		if (prev && start < prev->vm_end) {
			start = prev->vm_end;
		}

		if (start >= end) { goto loop_out; }

		if (prev) {
			vma = prev->vm_next;
		} else {
			vma = find_vma(mm, start);
		}
	}

loop_out:
	if (did_mark) {
		entry_vma->objmap.n_resident_pages = 0;

		for (vpage = entry_vma->vm_start; vpage < entry_vma->vm_end; vpage += PAGE_SIZE) {
			if ((pte = vpage_to_pte(mm, vpage))) {
				if (pte_present(*pte)) {
					entry_vma->objmap.n_resident_pages += 1;
				}
				pte_unmap(pte);
			}
		}

		write_unlock(&(entry_vma->objmap.rwlock));
	}
out_up_write:
	up_write(&mm->mmap_sem);
	mmput(mm);
out:
	return status;
}

ssize_t proc_object_map_controller_write(struct file *file, const char __user *buf, size_t count, loff_t *ppos) {
	struct object_map_controller_info *info_p, info;
	ssize_t                            status;
	struct task_struct                *task;

	if (count != sizeof(struct object_map_controller_info)) {
		DBUG("write payload was not the correct size\n");
		return -EINVAL;
	}

	info_p = (struct object_map_controller_info*)buf;

	if (copy_from_user(&info, info_p, sizeof(info)) != 0) {
		DBUG("error copying info from user space to kernel space\n");
		return -EFAULT;
	}

	if (info._magic != CONTROLLER_MAGIC) {
		DBUG("write payload has incorrect magic value\n");
		return -EINVAL;
	}

	status = -ENOENT;
	task   = get_proc_task(file->f_inode);
	if (!task) {
		DBUG("could not get task\n");
		count = status;
		goto out;
	}

	status = -EACCES;
	if (!ptrace_may_access(task, PTRACE_MODE_READ_FSCREDS)) {
		count = status;
		goto out_put_task;
	}

	switch (info.cmd) {
	case CONTROLLER_CMD_ADD:
		if (info.range_end <= info.range_start) {
			DBUG("range_end >= range_start\n");
			status = -EINVAL;
			break;
		}

		if (((unsigned long long)info.range_start) & (PAGE_SIZE - 1)
				||  ((unsigned long long)info.range_end)   & (PAGE_SIZE - 1)) {
			DBUG("misaligned range value(s)\n");
			status = -EINVAL;
			break;
		}

		status = object_map_controller_add_range(&info, task);
		break;
	case CONTROLLER_CMD_DEL:
		if (((unsigned long long)info.range_start) & (PAGE_SIZE - 1)) {
			DBUG("misaligned range value(s)\n");
			status = -EINVAL;
			break;
		}

		status = object_map_controller_del_range(&info, task);
		break;
	case CONTROLLER_CMD_SETNODE:
		if (((unsigned long long)info.range_start) & (PAGE_SIZE - 1)) {
			DBUG("misaligned range value(s)\n");
			status = -EINVAL;
			break;
		}
		if (info.node < 0) {
			DBUG("negative node value\n");
			status = -EINVAL;
			break;
		}

		status = object_map_controller_set_node(&info, task);
		break;
	default:
		DBUG("invalid command value\n");
		return -EINVAL;
	}

	if (status < 0) {
		count = status;
	}

out_put_task:
	put_task_struct(task);

out:
	return count;
}

static struct dentry *
proc_object_map_controller_instantiate(struct dentry *dentry,
		struct task_struct *task, const void *ptr) {
	struct proc_inode *ei;
	struct inode *inode;

	inode = proc_pid_make_inode(dentry->d_sb, task, S_IWUSR);

	if (!inode)
		return ERR_PTR(-ENOENT);

	ei = PROC_I(inode);

	inode->i_op   = &proc_object_map_controller_inode_operations;
	inode->i_fop  = &proc_object_map_controller_file_operations;
	inode->i_size = 64;

	d_set_d_op(dentry, &tid_object_map_controller_dentry_operations);
	return d_splice_alias(inode, dentry);
}



static int object_map_d_revalidate(struct dentry *dentry, unsigned int flags) {
	unsigned long vm_start, vm_end;
	struct vm_area_struct *vma;
	int good = 0;
	struct mm_struct *mm = NULL;
	struct task_struct *task;
	struct inode *inode;
	int status = 0;

	if (flags & LOOKUP_RCU)
		return -ECHILD;

	inode = d_inode(dentry);
	task = get_proc_task(inode);
	if (!task)
		goto out_notask;

	mm = mm_access(task, PTRACE_MODE_READ_FSCREDS);
	if (IS_ERR_OR_NULL(mm))
		goto out;

	if (strcmp(dentry->d_name.name, "controller") == 0) {
		good = 1;
	} else if (!dname_to_vma_addr(dentry, &vm_start, &vm_end)) {
		status = down_read_killable(&mm->mmap_sem);
		if (!status) {
			vma = find_exact_vma(mm, vm_start, vm_end);
			/* @vma */
			if (vma && vma->is_object_map_entry) {
				good = 1;
			}
			up_read(&mm->mmap_sem);
		}
	}

	mmput(mm);

	if (good) {
		task_dump_owner(task, 0, &inode->i_uid, &inode->i_gid);

		security_task_to_inode(task, inode);
		status = 1;
	}

out:
	put_task_struct(task);

out_notask:
	return status;
}

struct object_map_entry_info {
	unsigned long    start;
	unsigned long    end;
	/*     fmode_t        mode; */
};

static int
proc_object_map_readdir(struct file *file, struct dir_context *ctx) {
	struct vm_area_struct *vma;
	struct task_struct *task;
	struct mm_struct *mm;
	unsigned long nr_files, pos, i;
	GENRADIX(struct object_map_entry_info) fa;
	struct object_map_entry_info *p;
	int ret;
	char buf[4 * sizeof(long) + 2];    /* max: %lx-%lx\0 */
	unsigned int len;


	genradix_init(&fa);

	ret = -ENOENT;
	task = get_proc_task(file_inode(file));
	if (!task)
		goto out;

	ret = -EACCES;
	if (!ptrace_may_access(task, PTRACE_MODE_READ_FSCREDS))
		goto out_put_task;

	ret = 0;
	if (!dir_emit_dots(file, ctx))
		goto out_put_task;

	mm = get_task_mm(task);
	if (!mm)
		goto out_put_task;

	ret = down_read_killable(&mm->mmap_sem);
	if (ret) {
		mmput(mm);
		goto out_put_task;
	}

	nr_files = 0;

	/*
	 * We need two passes here:
	 *
	 *  1) Collect vmas of mapped files with mmap_sem taken
	 *  2) Release mmap_sem and instantiate entries
	 *
	 * otherwise we get lockdep complained, since filldir()
	 * routine might require mmap_sem taken in might_fault().
	 */

	for (vma = mm->mmap, pos = 2; vma; vma = vma->vm_next) {
		/* @vma */
		if (!vma->is_object_map_entry) {
			continue;
		}

		if (++pos <= ctx->pos)
			continue;

		p = genradix_ptr_alloc(&fa, nr_files++, GFP_KERNEL);
		if (!p) {
			ret = -ENOMEM;
			up_read(&mm->mmap_sem);
			mmput(mm);
			goto out_put_task;
		}

		p->start = vma->vm_start;
		p->end   = vma->vm_end;
	}

	up_read(&mm->mmap_sem);
	mmput(mm);

	for (i = 0; i < nr_files; i++) {
		p = genradix_ptr(&fa, i);
		len = snprintf(buf, sizeof(buf), "%lx-%lx", p->start, p->end);
		if (!proc_fill_cache(file, ctx,
					buf, len,
					proc_object_map_instantiate,
					task,
					NULL)) {
			goto out_put_task;
		}
		ctx->pos++;
	}

	if (++pos <= ctx->pos) {
		goto out_put_task;
	}

	if (!proc_fill_cache(file, ctx,
				"controller", 10,
				proc_object_map_controller_instantiate,
				task,
				NULL)) {
		goto out_put_task;
	}
	ctx->pos++;

out_put_task:
	put_task_struct(task);
out:
	genradix_free(&fa);
	return ret;
}

static const struct file_operations proc_object_map_operations = {
	.read           = generic_read_dir,
	.iterate_shared = proc_object_map_readdir,
	.llseek         = generic_file_llseek,
};

#undef OBJECT_MAP_WRITE_MAX_BYTES
#undef CONTROLLER_CMD_DEL
#undef CONTROLLER_CMD_ADD
#undef CONTROLLER_MAGIC
#undef INFO
#undef DBUG
#endif /* CONFIG_OBJECT_MAP */
